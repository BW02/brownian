import Brownian as br
import pandas as pd
import numpy as np

# Generate some Brownian Motion of a fixed Particle in 2d space

x0=np.array((2,2))
n = 50
dt = 1
delta = 4
r = br.brown(x0, n, dt, delta, out=None)

# Convert Brownian motion array to CSV and create Panda Dataframe from CSV

np.savetxt("brownian.csv", r, delimiter=",")
motion = pd.read_csv("brownian.csv",sep=",",header=None)

# Now we test if the csv file is indeed of a particle moving with brownian motion through space
# We know n (number of measurements) and assume that time interval elapsed between each measure is constant
# First, we calculate the distance between each two consecutive measures

distance = motion-motion.shift(periods=1, axis="columns").fillna(0)
del distance[0]

# Now we compute the expected mean (should be close to (0,0)) and expected standart deviation of the motion for the given (constant) time interval
# Both mean and deviation should be dimension independent (thus should lie close to the diagonal)

mean = distance.mean(axis=1)[0] #
std = distance.std(axis=1,ddof=0)[0]
print(distance.mean(axis=1),distance.std(axis=1,ddof=0))

# Finally, we check how the consecutive distances allocate around the mean 0.
# This way, we can find out if they distribute as we would expect from a gaussian distribution with the calculated
#deviation

z = [(distance <= i*std + mean) & (distance >= -i*std + mean) for i in range(1,4)]
for k in range(3):
    print("Relative Anzahl der Bewegungen im Intervall der Abweichung ", +-(k+1)*std, "vom Erwartungswert", mean ,":",  1/n*np.array([z[k].iloc[0].sum(),z[k].iloc[1].sum()]))
