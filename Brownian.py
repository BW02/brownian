from math import sqrt
import numpy as np

def brown(x0, n, dt, delta, out=None):

    x0 = np.asarray(x0)
    r = np.random.multivariate_normal(np.zeros(x0.size),delta*sqrt(dt)*np.eye(x0.size),(n)).T
    if out is None:
        out = np.empty(r.shape)
    np.cumsum(r, axis=1, out=out)
    out += np.expand_dims(x0, axis=-1)

    return np.c_[x0,out]

